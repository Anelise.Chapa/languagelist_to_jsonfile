import { BrowserModule } from '@angular/platform-browser';

import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';


import { AppComponent } from './app.component';

import { FormsModule } from '@angular/forms'; // <-- here
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { SearchService } from './app.component';
import { ArrayToJsonComponent } from './array-to-json/array-to-json.component';

@NgModule({
  declarations: [
    AppComponent,
    ArrayToJsonComponent
    
  ],
  imports: [
    BrowserModule, 
    FormsModule, // <-- here
    RoundProgressModule, // <-- and here, 
    HttpClientModule
  ],
  providers: [SearchService],
  bootstrap: [AppComponent]
})
export class AppModule { }
