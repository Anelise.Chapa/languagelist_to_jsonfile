import { TestBed } from '@angular/core/testing';

import { ArrayToJsonService } from './array-to-json.service';

describe('ArrayToJsonService', () => {
  let service: ArrayToJsonService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ArrayToJsonService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
