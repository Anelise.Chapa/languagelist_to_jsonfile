import { Component, OnInit, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/takeWhile';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/toPromise';
import { HttpClient, HttpClientModule} from '@angular/common/http'

class SearchItem {
  constructor(
    public track: string,
    public artist: string,
    public link: string,
    public thumbnail: string,
    public artistId: string
  ) {}
}

@Injectable()
export class SearchService {
  apiRoot: string = "https://itunes.apple.com/search";
  results: SearchItem[];
  loading: boolean;

  constructor(private http: HttpClient) {
    this.results = [];
    this.loading = false;
  }

  search(term: string) {
      let apiURL = `${this.apiRoot}?term=${term}&media=music&limit=20`;
      const promise = this.http.get(apiURL).toPromise();
      promise.then((data)=>{
        console.log("Promise resolved with: " + JSON.stringify(data));
      }).catch((error)=>{
        console.log("Promise rejected with " + JSON.stringify(error));
      });
    return promise;
  }
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {

  title   = 'Electron timer by ANE';
  query_start : string;
  query_end : string;
  artists : any;
  max     = 1;
  current = 0;
  currentArtist : any; 
  private loading: boolean = false;
  

  //showArtist
  showArtist( item: any) {
    this.query_start = item.name
    this.query_end = item.name;
    this.currentArtist = item;
  }

  //constructor
  constructor( private http: HttpClient, public itunes: SearchService) {
    this.query_start = '';
    this.query_end = '';
  }

  ngOnInit() : void {
    this.http.get<Object>('../assets/data.json').subscribe( 
      data => {
        this.artists = data;
    })
  }

  /// Start the timer
  start() {
    const interval = Observable.interval(100);
    
        interval
          .takeWhile(_ => !this.isFinished )
          .do(i => this.current += 0.1)
          .subscribe();
  }

   /// finish timer
  finish() {
    this.current = this.max;
  }

  /// reset timer
  reset() {
    this.current = 0;
  }

  /// Getters to prevent NaN errors

  get maxVal() {
    return isNaN(this.max) || this.max < 0.1 ? 0.1 : this.max;
  }

  get currentVal() {
    return isNaN(this.current) || this.current < 0 ? 0 : this.current;
  }

  get isFinished() {
    return this.currentVal >= this.maxVal;
  }

  doSearch(term: string) {
    this.loading = true;
    this.itunes.search(term).then(_ => (this.loading = false)
    );
  }

}
