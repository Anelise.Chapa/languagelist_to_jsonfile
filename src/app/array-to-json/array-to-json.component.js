"use strict";
/***********************************************************
* ARRAY-TO-JSON Component
* Author: Anelise Chapa
************************************************************
* Description:
* Defines html behavior in 2 functions uploading a file and
* from output generated json, creates a button that is used
* to copy to clipboard
*
************************************************************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrayToJsonComponent = void 0;
/*****************************************************
* Imports
******************************************************/
var core_1 = require("@angular/core");
var array_to_json_service_1 = require("../array-to-json.service");
var ngx_clipboard_1 = require("ngx-clipboard");
require("bootstrap");
/*****************************************************
* Component Definition
******************************************************/
var ArrayToJsonComponent = /** @class */ (function () {
    function ArrayToJsonComponent(to_json, clipboardApi) {
        this.to_json = to_json;
        this.clipboardApi = clipboardApi;
    }
    ArrayToJsonComponent.prototype.ngOnInit = function () {
    };
    /************************************************************
     * Function: languageFileUploaded
     * Description: When input of files detected, call on a promise
     * to wait for end of load of all files to then append all
     * files into a single string. Call on json service to
     * convert string to json file by language
     *
     * Inputs  :
     * input: event -> triggered by the upload of files into html
     *
     * Outputs : None
     *
     *************************************************************/
    ArrayToJsonComponent.prototype.languageFileUploaded = function (event) {
        var e_1, _a;
        var _this = this;
        var item = event.target;
        var json_generation_service = new array_to_json_service_1.ArrayToJsonService();
        var promises = [];
        var _loop_1 = function (file) {
            var filePromise = new Promise(function (resolve) {
                var reader = new FileReader();
                reader.readAsText(file);
                reader.onloadend = function () { return resolve(reader.result); };
            });
            promises.push(filePromise);
        };
        try {
            for (var _b = __values(event.target.files), _c = _b.next(); !_c.done; _c = _b.next()) {
                var file = _c.value;
                _loop_1(file);
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_1) throw e_1.error; }
        }
        Promise.all(promises).then(function (fileContents) {
            var all_files = "";
            for (var i = 0; i < fileContents.length; i++) {
                all_files += fileContents[i];
            }
            json_generation_service.loadLanguageFile(all_files);
            _this.generated_json = json_generation_service.getJsonFile();
        });
    };
    /************************************************************
   * Function: copyText
   * Description: copy to clipboard generated text
   *
   * Inputs  : None
   *
   * Outputs : None
   *
   *************************************************************/
    ArrayToJsonComponent.prototype.copyText = function () {
        this.clipboardApi.copyFromContent(this.generated_json);
    };
    ArrayToJsonComponent = __decorate([
        core_1.Component({
            selector: 'app-array-to-json',
            templateUrl: './array-to-json.component.html'
        })
        /*****************************************************
        * Class
        ******************************************************/
        ,
        __metadata("design:paramtypes", [array_to_json_service_1.ArrayToJsonService,
            ngx_clipboard_1.ClipboardService])
    ], ArrayToJsonComponent);
    return ArrayToJsonComponent;
}());
exports.ArrayToJsonComponent = ArrayToJsonComponent;
//# sourceMappingURL=array-to-json.component.js.map