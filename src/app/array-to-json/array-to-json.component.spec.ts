import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ArrayToJsonComponent } from './array-to-json.component';

describe('ArrayToJsonComponent', () => {
  let component: ArrayToJsonComponent;
  let fixture: ComponentFixture<ArrayToJsonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ArrayToJsonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ArrayToJsonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
