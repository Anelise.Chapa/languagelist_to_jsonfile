/***********************************************************
* ARRAY-TO-JSON Component
* Author: Anelise Chapa 
************************************************************
* Description:
* Defines html behavior in 2 functions uploading a file and
* from output generated json, creates a button that is used
* to copy to clipboard
* 
************************************************************/

/*****************************************************
* Imports
******************************************************/

import { Component, OnInit } from '@angular/core';
import { ArrayToJsonService }    from '../array-to-json.service';
import { ClipboardService } from 'ngx-clipboard';
import "bootstrap";

/*****************************************************
* Component Definition
******************************************************/

@Component({
  selector: 'app-array-to-json',
  templateUrl: './array-to-json.component.html'
})

/*****************************************************
* Class
******************************************************/

export class ArrayToJsonComponent implements OnInit {

  generated_json: string;
  constructor( private to_json: ArrayToJsonService,
               private clipboardApi: ClipboardService) { }

  ngOnInit(): void {
  }

/************************************************************
 * Function: languageFileUploaded  
 * Description: When input of files detected, call on a promise
 * to wait for end of load of all files to then append all 
 * files into a single string. Call on json service to 
 * convert string to json file by language
 *
 * Inputs  : 
 * input: event -> triggered by the upload of files into html
 * 
 * Outputs : None
 * 
 *************************************************************/ 

  public languageFileUploaded(event: any) 
  {
      let item = event.target;
      let json_generation_service = new ArrayToJsonService();
      let promises = [];
      for (let file of event.target.files) {
          let filePromise = new Promise(resolve => {
              var reader: FileReader = new FileReader();
              reader.readAsText(file);
              reader.onloadend = () => resolve(reader.result as string);
          });
          promises.push(filePromise);
      }
      Promise.all(promises).then(fileContents => {
        let all_files : string = ""
        for(let i = 0; i< fileContents.length; i++) {
          all_files += fileContents[i]
        }
        json_generation_service.loadLanguageFile(all_files)
        this.generated_json = json_generation_service.getJsonFile()
      });   
  }

  /************************************************************
 * Function: copyText  
 * Description: copy to clipboard generated text
 *
 * Inputs  : None
 * 
 * Outputs : None
 * 
 *************************************************************/ 

  public copyText() {
    this.clipboardApi.copyFromContent(this.generated_json)
  }
}
