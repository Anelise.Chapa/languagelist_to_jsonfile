"use strict";
/***********************************************************
* ARRAY-TO-JSON Service
* Author: Anelise Chapa
************************************************************
* Description:
* Service that handles information in string form and
* generates a json file separated by languages
*
************************************************************/
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ArrayToJsonService = void 0;
/*****************************************************
* Imports
******************************************************/
var core_1 = require("@angular/core");
var language_files_specs_1 = require("./array-to-json/language_files_specs");

/*****************************************************
* Class
******************************************************/
var ArrayToJsonService = /** @class */ (function () {
    function ArrayToJsonService() {
        this.json_obj = {
            table: []
        };
    }
    /************************************************************
     * Function: removeForbiddenCharacters
     * Description: removes certain characters from string
     *
     * Inputs  :
     * input: String -> to be modified
     *
     * Outputs :
     * input: string -> returns modified string
     *
     *************************************************************/
    ArrayToJsonService.prototype.removeForbiddenCharacters = function (input) {
        var e_1, _a;
        var forbiddenChars = ['\r', 'L"', '\t', ',', '"'];
        try {
            for (var forbiddenChars_1 = __values(forbiddenChars), forbiddenChars_1_1 = forbiddenChars_1.next(); !forbiddenChars_1_1.done; forbiddenChars_1_1 = forbiddenChars_1.next()) {
                var char = forbiddenChars_1_1.value;
                input = input.split(char).join('');
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (forbiddenChars_1_1 && !forbiddenChars_1_1.done && (_a = forbiddenChars_1.return)) _a.call(forbiddenChars_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return input;
    };
    /************************************************************
     * Function: addToJsonTable
     * Description: Adds to array json_obj.table nested key-value
     * pairs to all JSON_KEY type keys having as key the name
     * of the language specified in the parameter
     *
     * Inputs  :
     * arr -> array containing key type in the same order
     * as LanguageConstants.JSON_KEYS[i] to append to key
     * my_language_name: String -> name of the language to add as key
     *
     * Outputs : None
     *
     *************************************************************/
    ArrayToJsonService.prototype.addToJsonTable = function (arr, my_language_name) {
        var _a, _b;
        var i = 0;
        while (i < language_files_specs_1.LanguageConstants.JSON_KEYS.length) {
            this.json_obj.table.push((_a = {}, _a[language_files_specs_1.LanguageConstants.JSON_KEYS[i]] = (_b = {}, _b[my_language_name] = arr[i], _b), _a));
            i++;
        }
        console.log("Finished %s file of length %i", my_language_name, arr.length);
    };
    /************************************************************
     * Function: inputAllElements
     * Description:
     * Reads a string containing multiple language files,
     * only from a start to finish indicated by parameters to
     * form an array of elements inside these sof and eof
     *
     * Inputs  :
     * filestring : string -> a string containing all language files
     * sof: number -> start of language file to read
     * eof: number -> end of language file to read
     *
     * Outputs :
     * txt_arr: [] -> return an aray containing all elements
     * read from language txt file
     *
     *************************************************************/
    ArrayToJsonService.prototype.inputAllElements = function (file_string, sof, eof) {
        var txt_arr = [];
        var my_newline;
        //first element in all language is the same, find the newline character after first element
        my_newline = file_string.indexOf(language_files_specs_1.LanguageConstants.FIRST_ELEMENT, sof);
        //iterate all language file until end of file is found
        while (my_newline <= eof) {
            //find newline character
            var my_eol = file_string.indexOf(language_files_specs_1.LanguageConstants.NEWLINE, my_newline);
            //find string starting at last newline
            var my_str = file_string.substring(my_newline, my_eol);
            //add 1 to newline character to find next element
            my_newline = my_eol + 1;
            //all read elements have extra characters. clean
            my_str = this.removeForbiddenCharacters(my_str);
            txt_arr.push(my_str);
        }
        return txt_arr;
    };
    /************************************************************
     * Function: loadLanguageFile
     * Description:
     * inputs a string file containing all language files ready
     * to be formatted to a json file. all equal json keys will
     * be joined into 1.
     *
     * Inputs  :
     * filestring: String -> a string containing all language files
     * concatenated.
     *
     * Outputs : None
     *
     *************************************************************/
    ArrayToJsonService.prototype.loadLanguageFile = function (fileString) {
        var e_2, _a;
        var lang_eof;
        var txt_arr = [];
        try {
            //Iterate through all languages
            for (var language_Dictionary_1 = __values(language_files_specs_1.language_Dictionary), language_Dictionary_1_1 = language_Dictionary_1.next(); !language_Dictionary_1_1.done; language_Dictionary_1_1 = language_Dictionary_1.next()) {
                var lang = language_Dictionary_1_1.value;
                //if language start of file is found
                var lang_sof = fileString.indexOf(lang.SOF);
                if (lang_sof != language_files_specs_1.LanguageConstants.INDEX_NOT_FOUND) {
                    //find last position of found language file
                    lang_eof = fileString.indexOf(language_files_specs_1.LanguageConstants.ALL_EOF, lang_sof);
                    txt_arr = this.inputAllElements(fileString, lang_sof, lang_eof);
                    this.addToJsonTable(txt_arr, lang.language_text);
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (language_Dictionary_1_1 && !language_Dictionary_1_1.done && (_a = language_Dictionary_1.return)) _a.call(language_Dictionary_1);
            }
            finally { if (e_2) throw e_2.error; }
        }
    };
    /************************************************************
     * Function: getJsonFile
     * Description:
     * Return formatted json file
     *
     * Inputs  : None
     *
     * Outputs : String
     * Return  : json string
     *
     *************************************************************/
    ArrayToJsonService.prototype.getJsonFile = function () {
        //iteration to concatenate duplicate key,value pairs into a single key
        var result = this.json_obj.table.reduce(function (obj, item) {
            Object.keys(item).forEach(function (key) {
                if (!obj[key]) {
                    obj[key] = [].concat([], item[key]);
                }
                else {
                    obj[key].push(item[key]);
                }
            });
            return obj;
        }, {});
        //formatted array: format into json
        var json = JSON.stringify(result, null, "\t");
        return json;
    };
    ArrayToJsonService = __decorate([
        core_1.Injectable({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], ArrayToJsonService);
    return ArrayToJsonService;
}());
exports.ArrayToJsonService = ArrayToJsonService;
//# sourceMappingURL=array-to-json.service.js.map