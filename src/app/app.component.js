"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AppComponent = exports.SearchService = void 0;
var core_1 = require("@angular/core");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/observable/interval");
require("rxjs/add/operator/map");
require("rxjs/add/operator/takeWhile");
require("rxjs/add/operator/do");
require("rxjs/add/operator/toPromise");
var http_1 = require("@angular/common/http");
var SearchItem = /** @class */ (function () {
    function SearchItem(track, artist, link, thumbnail, artistId) {
        this.track = track;
        this.artist = artist;
        this.link = link;
        this.thumbnail = thumbnail;
        this.artistId = artistId;
    }
    return SearchItem;
}());
var SearchService = /** @class */ (function () {
    function SearchService(http) {
        this.http = http;
        this.apiRoot = "https://itunes.apple.com/search";
        this.results = [];
        this.loading = false;
    }
    SearchService.prototype.search = function (term) {
        var apiURL = this.apiRoot + "?term=" + term + "&media=music&limit=20";
        var promise = this.http.get(apiURL).toPromise();
        promise.then(function (data) {
            console.log("Promise resolved with: " + JSON.stringify(data));
        }).catch(function (error) {
            console.log("Promise rejected with " + JSON.stringify(error));
        });
        return promise;
    };
    SearchService = __decorate([
        core_1.Injectable(),
        __metadata("design:paramtypes", [http_1.HttpClient])
    ], SearchService);
    return SearchService;
}());
exports.SearchService = SearchService;
var AppComponent = /** @class */ (function () {
    //constructor
    function AppComponent(http, itunes) {
        this.http = http;
        this.itunes = itunes;
        this.title = 'Electron timer by ANE';
        this.max = 1;
        this.current = 0;
        this.loading = false;
        this.query_start = '';
        this.query_end = '';
    }
    //showArtist
    AppComponent.prototype.showArtist = function (item) {
        this.query_start = item.name;
        this.query_end = item.name;
        this.currentArtist = item;
    };
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.http.get('../assets/data.json').subscribe(function (data) {
            _this.artists = data;
        });
    };
    /// Start the timer
    AppComponent.prototype.start = function () {
        var _this = this;
        var interval = Observable_1.Observable.interval(100);
        interval
            .takeWhile(function (_) { return !_this.isFinished; })
            .do(function (i) { return _this.current += 0.1; })
            .subscribe();
    };
    /// finish timer
    AppComponent.prototype.finish = function () {
        this.current = this.max;
    };
    /// reset timer
    AppComponent.prototype.reset = function () {
        this.current = 0;
    };
    Object.defineProperty(AppComponent.prototype, "maxVal", {
        /// Getters to prevent NaN errors
        get: function () {
            return isNaN(this.max) || this.max < 0.1 ? 0.1 : this.max;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AppComponent.prototype, "currentVal", {
        get: function () {
            return isNaN(this.current) || this.current < 0 ? 0 : this.current;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AppComponent.prototype, "isFinished", {
        get: function () {
            return this.currentVal >= this.maxVal;
        },
        enumerable: false,
        configurable: true
    });
    AppComponent.prototype.doSearch = function (term) {
        var _this = this;
        this.loading = true;
        this.itunes.search(term).then(function (_) { return (_this.loading = false); });
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        }),
        __metadata("design:paramtypes", [http_1.HttpClient, SearchService])
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map