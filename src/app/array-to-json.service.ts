/***********************************************************
* ARRAY-TO-JSON Service
* Author: Anelise Chapa 
************************************************************
* Description:
* Service that handles information in string form and 
* generates a json file separated by languages
* 
************************************************************/

/*****************************************************
* Imports
******************************************************/

import { Injectable } from '@angular/core';
import { LanguageConstants, language_Dictionary, LANGUAGE_ID } from './array-to-json/language_files_specs';


/*****************************************************
* Class
******************************************************/

@Injectable({
  providedIn: 'root'
})
export class ArrayToJsonService {
 
  json_obj = {
    table: [
    ]
  };

  constructor() {  
  }

/************************************************************
 * Function: removeForbiddenCharacters  
 * Description: removes certain characters from string  
 *
 * Inputs  : 
 * input: String -> to be modified
 * 
 * Outputs :    
 * input: string -> returns modified string
 * 
 *************************************************************/ 

  private removeForbiddenCharacters(input) 
  {
    let forbiddenChars = ['\r', 'L"','\t',',','"']
    
    for (let char of forbiddenChars){
        input = input.split(char).join('');
    }
    return input
  }

/************************************************************
 * Function: addToJsonTable  
 * Description: Adds to array json_obj.table nested key-value 
 * pairs to all JSON_KEY type keys having as key the name 
 * of the language specified in the parameter
 *
 * Inputs  : 
 * arr -> array containing key type in the same order
 * as LanguageConstants.JSON_KEYS[i] to append to key
 * my_language_name: String -> name of the language to add as key
 * 
 * Outputs : None 
 * 
 *************************************************************/ 

  private addToJsonTable( arr: any, my_language_name: string)
  {
    var i = 0

    while(i < LanguageConstants.JSON_KEYS.length) { 
      this.json_obj.table.push({[LanguageConstants.JSON_KEYS[i]]: {[my_language_name]: arr[i]}})
      i++
    }
    
    console.log("Finished %s file of length %i", my_language_name, arr.length);
  }

/************************************************************
 * Function: inputAllElements  
 * Description:   
 * Reads a string containing multiple language files, 
 * only from a start to finish indicated by parameters to 
 * form an array of elements inside these sof and eof
 *
 * Inputs  : 
 * filestring : string -> a string containing all language files
 * sof: number -> start of language file to read
 * eof: number -> end of language file to read
 * 
 * Outputs : 
 * txt_arr: [] -> return an aray containing all elements 
 * read from language txt file     
 * 
 *************************************************************/ 

  private inputAllElements(file_string: string, sof: number, eof: number) {
    let txt_arr = []
    var my_newline: number 

    //first element in all language is the same, find the newline character after first element
    my_newline = file_string.indexOf(LanguageConstants.FIRST_ELEMENT, sof);

    //iterate all language file until end of file is found
    while(my_newline <= eof) {

      //find newline character
      let my_eol = file_string.indexOf(LanguageConstants.NEWLINE, my_newline)
      
      //find string starting at last newline
      let my_str = file_string.substring(my_newline, my_eol)
      
      //add 1 to newline character to find next element
      my_newline = my_eol + 1;

      //all read elements have extra characters. clean
      my_str = this.removeForbiddenCharacters(my_str)
      txt_arr.push(my_str)
    }
    return txt_arr
  }
  

/************************************************************
 * Function: loadLanguageFile  
 * Description:   
 * inputs a string file containing all language files ready
 * to be formatted to a json file. all equal json keys will 
 * be joined into 1. 
 *
 * Inputs  : 
 * filestring: String -> a string containing all language files 
 * concatenated.
 * 
 * Outputs : None    
 * 
 *************************************************************/ 

  public loadLanguageFile(fileString: string){  
    let lang_eof: number
    let txt_arr = []

    //Iterate through all languages
    for(var lang of language_Dictionary) {
      
      //if language start of file is found
      let lang_sof = fileString.indexOf(lang.SOF)
      if(lang_sof != LanguageConstants.INDEX_NOT_FOUND) {
        
        //find last position of found language file
        lang_eof = fileString.indexOf(LanguageConstants.ALL_EOF, lang_sof)
        txt_arr = this.inputAllElements(fileString, lang_sof, lang_eof)
        this.addToJsonTable( txt_arr, lang.language_text)
      }
    }
  }

/************************************************************
 * Function: getJsonFile  
 * Description:   
 * Return formatted json file 
 *
 * Inputs  : None
 * 
 * Outputs : String    
 * Return  : json string
 * 
 *************************************************************/ 

  public getJsonFile(): string {
    //iteration to concatenate duplicate key,value pairs into a single key
    let result = this.json_obj.table.reduce(function(obj, item){
      Object.keys(item).forEach(function(key){
        if(!obj[key]) {
          obj[key] = [].concat([], item[key])
        }
        else {
             obj[key].push(item[key]);
        }
      });
      return obj;
    },{});
    //formatted array: format into json
    var json = JSON.stringify(result, null, "\t")
    return json
  }

}
