import { TestBed } from '@angular/core/testing';

import { JsonToArraysService } from './json-to-arrays.service';

describe('JsonToArraysService', () => {
  let service: JsonToArraysService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(JsonToArraysService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
