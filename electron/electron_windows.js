
const fs = require('fs')
const {app, BrowserWindow} = require('electron')
const url = require("url");
const path = require("path");

const args = process.argv.slice(1),
serve = args.some(val => val === '--serve');

let win;
let brows;
function createBrowser () {

  // Create the browser window.
  brows = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      nodeIntegration: true,
      allowRunningInsecureContent: (serve) ? true : false,
      contextIsolation: false,  // false if you want to run e2e test with Spectron
      enableRemoteModule : true // true if you want to run e2e test with Spectron or use remote module in renderer context (ie. Angular)
    }
  })
  
  if (serve) {
    require('electron-reload')(__dirname, {
    electron: require(path.join(__dirname, '/../node_modules/electron'))
    });
    brows.loadURL('http://localhost:4200');
} 
else 
{
    // Path when running electron executable
    let pathIndex = `../dist/angularElectron/index.html`;   // This is running

    if (fs.existsSync(path.join(__dirname, '../dist/index.html'))) 
    {
        // Path when running electron in local folder
        pathIndex = '../dist/index.html';
    }

    brows.loadURL(
        url.format({
        pathname: path.join(__dirname, pathIndex),  
        protocol: "file:",
        slashes: true
        })
        );
}
  
      
  //// uncomment below to open the DevTools.
  brows.webContents.openDevTools()

  // Event when the window is closed.
  brows.on('closed', function () {
    win = null
      })
}
  
// Create window on electron initialization
app.on('ready', createBrowser)

// Quit when all windows are closed.
app.on('window-all-closed', function () {

  // On macOS specific close process
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

  
